from sqlalchemy.orm import load_only


class BaseQuery(object):
    """
    TODO
    """
    def __init__(self, session, q):
        """
        :param sqlalchemy.orm.Session session:
        :param sqlalchemy.orm.Query q:
        """
        self.session = session
        self.q = q

    def __iter__(self):
        return iter(self.q)

    def __getattr__(self, name):
        return getattr(self.q, name)

    def only(self, *fields):
        return self.__class__(self.session, self.q.options(
            load_only(*fields)
        ))

    def exists(self):
        return self.count() > 0

    def get(self, field):
        return self.only(field).scalar()

    def chunks(self, chunk_size=200):
        return ChunkIterator(self, chunk_size)


class ChunkIterator(object):
    def __init__(self, query, chunk_size):
        """
        :param BaseQuery query:
        :param int chunk_size:
        """
        self.query = query
        self.chunk_size = chunk_size

    def __iter__(self):
        for offset in range(0, self.query.count(), self.chunk_size):
            yield from self.query \
                .offset(offset) \
                .limit(self.chunk_size)
